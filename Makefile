#2017-11-24 Added support for pdf download in eclipse
#Path to this subsystem's root directory
SUBSYSROOT := $(shell pwd)

# ***************** Generic docbuild.git auto-clone + s_docbuild symlink ***
# This also defines TMPCLONEROOT which optionally can be nondefault using BOOK_GLOBALCLONEROOT=yes or a path
include init.mk

# -----------------------------------------------------
#Path to DocBook make files and templates
DOCBOOKMAKE         = $(SUBSYSROOT)/s_docbuild/docmake
DOCBOOKTEMPLATE     = $(SUBSYSROOT)/s_docbuild/template

#Path to the OLINK database including leading part of file name (will add -$(FORMAT).db)
DOCBOOKOLINKDB_BASE = $(SUBSYSROOT)/s_docbuild/olinkdb/olink-targetdb-ose5-master

DOCBOOK_OLINKS ?= yes
DOCBOOK_FO_USEFOP ?= yes
DOCBOOK_TO_BOOKDIR ?= yes
DOCBOOK_CLEANTMP ?= yes

COMPONENTS := $(shell ls -d book-enea* )
# --------------------------------------------------------------
ifeq ($(VERBOSE),yes)
VERB :=
else
VERB := @
endif
MAKEFLAGS += --no-print-directory
MAKEFLAGS += --directory $(SUBSYSROOT)
MAKEFLAGS += --no-builtin-rules --no-builting-variables

# Skip xml validation to make it possible to include xml files with unresolved links
VALIDATE = no

# BL_LABEL is either given on make command line or using BOOK_VER which have defaults below or a timestamp

# ******************************************************************

# Export all parameters including those on the command line
export

.PHONY: doc books docusage init initbuild initpardoc dist


docusage:
	@echo 'make docusage                  #Shows this help text'
	@echo '  DOCBOOK_TOOLS_VERSIONS=yes   #Displays DocBook tools versions in this machine'
	@echo 'make doc                       #Builds doc. ALSO automatically does all inits if needed'
	@echo 'make dist BOOK_DIST_DIR=xx/doc #Builds doc as above and copies results to BOOK_DIST_DIR'
	@echo '                               #   Does not work with DOCBOOK_TO_BOOKDIR or BOOK_BUILD_DIR'
	@echo ' BOOK_DIST_ECLIPSE=yes         #Currently EclipseHelp formated doc will not be in BOOK_DIST_DIR'
	@echo '                               #   unless BOOK_DIST_ECLIPSE is set to yes'
	@echo 'make init                      #Init all needed init* below'
	@echo 'make initbuild                 #Init s_docbuild Docbook build system and central files'
	@echo 'make initpardoc                #Init s_docsrc_common with extracted parameters/'
	@echo 'make initissues                #Init known issues section from Jira'
	@echo ' DOCBOOK_CLEANTMP=no           #Option to keep temp files in doc/ and tmp/'
	@echo ' BOOK_GLOBALCLONEROOT=yes      #Option for all init above to clone all outside the doc directory'
	@echo ' BOOK_GLOBALCLONEROOT=otherpath #Option for all init above to clone all to given path'
	@echo 'make pullbuild                 #git pull in s_docbuild'
	@echo 'make clean                     #Clean results and all s_*, but not any external clones'
	@echo ''
	@echo 'Optional parameters for make doc:'
	@echo '  COMP=<book-directory>     #Component (book) to build. Book component names are book-*'
	@echo '                            #Default component/s:'
	@echo '    $(COMPONENTS)'
	@echo '  FORMAT=<format>           #One of: pdf'
	@echo '  BL_LABEL=<baseline>       #Becomes footer in book'
	@echo '  DOCBOOK_TO_BOOKDIR=no     #(default yes) Avoid moving result to book directory'
	@echo '                            #   and avoid erasing common doc and tmp directories'
	@echo '  BOOKFORCE=yes             #Force rebuilding (ignore dependency on file times or BL_LABEL)'
	@echo '                            #   Dependency only works if common doc directory is kept'
	@echo '  BOOKCONDITION="xx;yy;.."  #Include XML elements with condition any of xx or yy'
	@echo '                            #   (if rebuilding, BOOKFORCE=yes may be needed)'
	@echo '                            #   Empty=only default. none=none, all=everything.'
	@echo '  BOOKDEFAULTCONDITION      #Default conditions, if no BOOKCONDITION. Used in book-*/swcomp.mk'
	@echo '  SHOW_COMMENTS=yes         #For proofread. Unhide <remark>..</remark> comments Only PDF'
	@echo '  BOOKVERBOSE=yes           #Verbose info building books'
	@echo '  DOCBOOK_OLINKS=no         #Avoid the olink database in Makefile (in book not using it)'
	@echo '  DOCBOOK_OLINK_TARGETDB=only  #Build a target db for this book (for links into it)'
	@echo '  DOCBOOK_OLINK_TARGETDB=yes   #Build a target db AND build the book'
	@echo '                               #Master olinkdb defined in this Makefile, one per generated format, is:'
	@echo '   $(DOCBOOKOLINKDB_BASE)_*.db'
	@echo '  Typical examples:'
	@echo '     make doc                          Creates all books, all formats'
	@echo '     make doc COMP=book-xxxx FORMAT=html'
	@echo '     make doc COMP=book-xxxx FORMAT=pdf'
	@echo '     make doc BL_LABEL="Version 1.2.3"  Creates all with version in footers and front'
	@echo ''
	@echo '  Requires docbook-xsl-1.76.1 or later, docbook-xml 4.2, svg1.1, fop-1.0 + fop-hyph.jar'
	@echo '     fop + the separate fop-hyph can be found together in package "libfop-java"'
	@echo '     otherwise fetch fop-hyph.jar and place in same place as fop.jar'
	@echo '  Requires libxml2-2.7.8 or later, libxslt-1.1.26 or later'
	@echo '  Requires java machine to run fop (creating PDF). jar for optional FORMAT=eclipse'
	@echo '      Without jar, the optional EclipseHelp format can not be built'
	@echo '  tools_book_standalone.mk => libxml2, libxslt, and fop (and to catalog-el.xml)'
	@echo '  catalog-el.xml => svg, docbook-xml, docbook-xsl'
ifeq ($(DOCBOOK_TOOLS_VERSIONS),yes)
	$(VERB)$(MAKE) -f $(DOCBOOKMAKE)/tools_book_standalone.mk book_tools_versions
endif

# We rely on make doing these in order left to right
init: initbuild initpardoc
pullall: pullbuild

# If no COMP, iterate over books-* in COMPONENTS with make doc
ifeq ($(COMP),)
doc:
ifneq ($(filter book-%, $(COMPONENTS)),)
	$(VERB)for comp in $(filter book-%, $(COMPONENTS)); do \
	  $(MAKE) doc COMP=$$comp; \
	done
endif

else
include $(SUBSYSROOT)/$(COMP)/swcomp.mk

doc: books
	@#
endif

dist: doc
	@echo "Copying resulting built documents to $(BOOK_DIST_DIR)"
	$(VERB)if [ "$(BOOK_DIST_DIR)" = "" ]; then echo "ERROR: Missing BOOK_DIST_DIR parameter, typically shall be xxx/doc"; exit 10; fi
	$(VERB)if [ ! -d "`dirname $(BOOK_DIST_DIR)`" ]; then echo "ERROR: Missing parent for BOOK_DIST_DIR"; exit 10; fi
	$(VERB)if [ -f "$(BOOK_DIST_DIR)/Makefile" ]; then echo "ERROR: Wrong BOOK_DIST_DIR, contains a Makefile?"; exit 10; fi
	$(VERB)if [ ! -d "$(BOOK_DIST_DIR)" ]; then mkdir -p "$(BOOK_DIST_DIR)" ; fi
	$(VERB)for book in $(COMPONENTS); do \
	   if ls -d $$book/book*.pdf >/dev/null 2>&1;  then cp    --preserve=timestamps $$book/book*.pdf "$(BOOK_DIST_DIR)" ; fi ; \
	   if ls -d $$book/html >/dev/null 2>&1;       then cp -r --preserve=timestamps $$book/html      "$(BOOK_DIST_DIR)" ; fi ; \
	done

# Default FORMATs
ifeq ($(FORMAT),)
FORMAT=pdf html eclipse
endif


books: init
# BOOKPACKAGES is defined in all book-*/swcomp.mk
	$(VERB)ENEA_NFV_REL_VER=`egrep 'id="ENEA_NFV_REL_VER"' s_docsrc_common/pardoc-distro.xml | sed 's/.*<phrase>//;s/<\/phrase>.*//'` ; \
	       BOOKVER="$$ENEA_NFV_REL_VER" ; \
	 for book in $(BOOKPACKAGES); do \
	  for format in $(FORMAT); do \
	    $(MAKE) -f $(DOCBOOKMAKE)/make_docbook_standalone.mk BOOK=$$book FORMAT=$$format BOOK_VER="$$BOOKVER" books || exit 10; \
		if [ $$format = eclipse ]; then \
		if [ ! -f $$COMP/$$book.pdf ]; then \
	      $(MAKE) -f $(DOCBOOKMAKE)/make_docbook_standalone.mk BOOK=$$book FORMAT=pdf BOOK_VER="$$BOOKVER" books || exit 10; \
		  fi; \
		  cp $$COMP/$$book.pdf $$COMP/eclipse/plugins/com.enea.doc.book-*/. ; \
		fi ; \
	  done ; \
	 done
ifeq ($(DOCBOOK_TO_BOOKDIR),yes)
	 $(VERB)rm -rf doc tmp
else
ifeq ($(DOCBOOK_CLEANTMP),yes)
#        keep only doc/pdf/book-*.pdf
	$(VERB)rm -rf tmp 2>/dev/null ; rm -rf doc/pdf/book-*/
endif
endif

# cleaninit cleans ALL tmpcommon and all s_*
clean:  cleaninit
	@echo "Cleaning build results and temporary files"
	$(VERB)rm -rf doc tmp 2>/dev/null
ifneq ($(filter book-%, $(COMPONENTS)),)
	$(VERB)for comp in $(filter book-%, $(COMPONENTS)); do \
	  rm -rf $$comp/book*.pdf $$comp/html $$comp/eclipse 2>/dev/null; \
	done
	$(VERB)rm -rf doc tmp 2>/dev/null
endif

# ****************************************************************************
# ******************** SUPPORT FOR dynamic pardoc with added parameters ******

PATH_DOCSRC_COMMON    = $(SUBSYSROOT)/s_docsrc_common
NAME_DOCSRC_COMMON    = docsrc_common
TMPCLONEROOT_DOCSRC_COMMON := $(TMPCLONEROOT)/$(NAME_DOCSRC_COMMON)
TMPPARDOC                  := $(TMPCLONEROOT_DOCSRC_COMMON)/pardoc-distro.xml

initpardoc:   s_docsrc_common

cleanpardoc:
	@echo "Cleaning dynamic parameters"
	$(VERB)rm -rf $(TMPCLONEROOT_DOCSRC_COMMON) ; rm s_docsrc_common

s_docsrc_common:  $(TMPCLONEROOT_DOCSRC_COMMON)
	$(VERB)rm s_docsrc_common 2>/dev/null; ln -s $(TMPCLONEROOT_DOCSRC_COMMON) s_docsrc_common

$(TMPCLONEROOT_DOCSRC_COMMON):
	$(VERB)if [ ! -d "$(TMPCLONEROOT)" ] ; then mkdir -p "$(TMPCLONEROOT)" ; fi
	$(VERB)if [ ! -d $(TMPCLONEROOT_DOCSRC_COMMON) ]; then mkdir -p $(TMPCLONEROOT_DOCSRC_COMMON) ; fi
	@echo "**** Copy docsrc_common/ files to $(TMPCLONEROOT_DOCSRC_COMMON)"
	$(VERB)cat docsrc_common/pardoc-distro.xml >$(TMPPARDOC)
